
<h3><?= $otsikko ?></h3>
<div>
<?= anchor('admin/tallenna', 'Lisää uusi'); ?>  <!-- Itseään kutsuva, tallenna-metodia -->
</div>
<table class="table">
<?php foreach($tuoteryhmat as $tuoteryhma): ?>
   <tr>
      <td>
          <?=  $tuoteryhma['nimi']; ?>
      </td>
      <td>
          <?= anchor('admin/poista/' . $tuoteryhma['id'], 'Poista'); ?>
      </td>
   </tr>
   <!-- Yllä siis hakasulkeissa kerrotaan, mikä kenttä tuoteryhma-taulusta pitää tulostaa-->
<?php endforeach; ?>
</table>
