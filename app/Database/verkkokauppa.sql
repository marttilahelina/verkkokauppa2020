drop database if exists verkkokauppa;
create database verkkokauppa;
use verkkokauppa;

create table tuoteryhma (
    id int primary key auto_increment,
    nimi varchar(50) not null
);

insert into tuoteryhma (nimi) values ("Asusteet");
insert into tuoteryhma (nimi) values ("Urheiluvälineet");
insert into tuoteryhma (nimi) values ("Puhelimet");

create table tuote (
    id int primary key auto_increment,
    nimi varchar(100) not null,
    kuvaus text not null,
    hinta double(6,2),
    kuva varchar(50),
    varastomaara smallint unsigned,
    tuoteryhma_id int not null,
    index tuoteryhma_id (tuoteryhma_id),
    foreign key (tuoteryhma_id) references tuoteryhma(id)
    on delete restrict
);

create table tilasrivi (
    tilaus_id int not null,
    index tilaus_id()
)

insert into tuote (nimi, kuvaus, hinta, kuva, varastomaara, tuoteryhma_id)
values ('chili', 'polttava', 10, 'chili1.jpg', 200, 2);

insert into tuote (nimi, kuvaus, hinta, kuva, varastomaara, tuoteryhma_id)
values ('xampp', 'kuumottava', 5, 'xampp.PNG', 30, 3);

insert into tuote (nimi, kuvaus, hinta, kuva, varastomaara, tuoteryhma_id)
values ('pokaali', 'Hieno palkinto', 115, 'pokaali.jpg', 6, 2);