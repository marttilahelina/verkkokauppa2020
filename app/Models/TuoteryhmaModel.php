<?php 
namespace App\Models;

use CodeIgniter\Model;

class TuoteryhmaModel extends Model { //Modelin sisältämät valmiit koodit periytetään TuoteryhmaModel-luokalle
    protected $table = 'Tuoteryhma'; //$table-määrittely tulee Modelista, oltava tällä nimellä
    protected $allowedFields= ['nimi']; //hakasulkeissa kerrotaan save-metodille (valmiina Modelissa), mitkä kentät sallittuja

    public function haeTuoteryhmat() {
        return $this->findAll(); 
        //$this on tässä TuoteryhmaModel
        // toiminto findAll() valmiina Modelissa. Käytännössä 'select * from tuoteryhma'
        // CodeIgniterissä siis tietokantaoperaatiot tehdään Model-luokan metodeilla, ei SQL-lauseilla
    }

    public function poista($id) {
        // SQL: delete from tuoteryhma where id = 
        $this->where('id', $id);
        $this->delete();
    }


}

?>