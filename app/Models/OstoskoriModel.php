<?php namespace App\Models;

use CodeIgniter\Model;

class OstoskoriModel extends Model {

    private $tuoteModel = null;

    function __construct() {
        $session = \Config\Services::session(); //käynnistää istunnon
        $session->start();
        if(!isset($_SESSION['kori'])) { // luodaan ostoskori, jos ei ole jo valmiiksi
        $_SESSION['kori'] = array();
        }

        $this->tuoteModel = new TuoteModel(); // Modelissa voidaan luoda toisesta luokasta model!
    }

    public function ostoskori() {
        return $this->tuoteModel->haeTuotteet($_SESSION['kori']); // kutsutaan tuote-luokan modelia
    }

    /**
     * Lisää uuden tuotteen ostoskoriin.
     * 
     * @param int $tuote_id Ostoskoriin lisättävän tuotteen id.
     */
    public function lisaa($tuote_id) {
        array_push($_SESSION['kori'], $tuote_id);
    }

    public function tyhjenna() {
        $_SESSION['kori'] = null;
        $_SESSION['kori'] = array(); //tyhjennyksen jälkeen tyhjä taulukko valmiiksi
    }


}