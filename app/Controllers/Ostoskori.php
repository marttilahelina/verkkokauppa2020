<?php namespace App\Controllers;

use App\Models\TuoteryhmaModel;
// use App\Models\TuoteModel; // yksi käsittelijä käyttää kaikkia tarvitsemiaan modeleja
use App\Models\OstoskoriModel;

class Ostoskori extends BaseController
{
	private $tuoteryhmaModel= null; // tarvitaan aina, että navbariin voidaan luoda valikko
    //private $tuoteModel = null;
    private $ostoskoriModel = null;
	
    function __construct() {

	    $this->tuoteryhmaModel = new TuoteryhmaModel();
        //$this->tuoteModel = new TuoteModel();  
        $this->ostoskoriModel = new OstoskoriModel();  
    }

    public function index()
	{
       $data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
       $data['tuotteet'] = $this->ostoskoriModel->ostoskori();
       
       echo view('templates/header', $data); //huom. eri templatet ylläpitopuolelle ja asiakkaan puolelle!
	   echo view('ostoskori', $data);
	   echo view('templates/footer');	
	}
    
    public function lisaa($tuote_id) {
        $this->ostoskoriModel->lisaa($tuote_id); // istuntomuuttuja asetettu jo modelissa, ei tarvi enää tässä
        return redirect()->to(site_url('home/tuote/' . $tuote_id)); // palataan takaisin samalle tuote-sivulle
    }

    public function tyhjenna() {
        $this->ostoskoriModel->tyhjenna();
        return redirect()->to(site_url('ostoskori/index'));
    }

}