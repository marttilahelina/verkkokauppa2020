<?php namespace App\Controllers;

use App\Models\TuoteryhmaModel; // jotta saadaan Admin-käsittelijässä kutsuttua TuoteryhmaModel-modelia

class Admin extends BaseController //sivukutsu luo olion Admin-luokasta, sitä ei tehdä itse
{
	private $tuoteryhmaModel = null; //jäsenmuuttujaksi määritellään tuoteryhmaModel, metodien ja konstruktorin ulkopuolella

	function __construct() { // konstruktori, kun Admin-luokasta luodaan uusi model
		$this->tuoteryhmaModel = new TuoteryhmaModel(); // tällä luokalla on tuoteryhmaModel //tehdään olio TuoteryhmaModel-luokasta
	}


	public function index()
	{
		$data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat(); // kyselyn tiedot tallennetaan tuoteryhmat-taulukkoon
		$data['otsikko'] = 'Tuoteryhmät';
		echo view("templates/header_admin.php");
		echo view("admin/tuoteryhma.php", $data); // näyttää tuoteryhma.php -näkymässä $data-muuttujan sisältöä
		// Tässä $data-taulukkoon tallennetaan monenlaista tietoa, yo. lause näyttää ne kaikki, 
		//sekä otsikon että tuoteryhmät
		echo view("templates/footer.php");
	}

	public function tallenna() 
	{
		if (!$this->validate([ // Jos ei validoidu (metodi valmiina Modelissa), näytetään lomakesivu
			'nimi' => 'required|max_length[50]' // hakasulkeissa annetaan parametrinä taulukko, jota validoidaan
		])) { 
			echo view("templates/header_admin.php");
			echo view("admin/tuoteryhma_lomake"); // ei tarvitse laittaa .php-tiedostopäätettä
			echo view("templates/footer.php");
		}
		else {
			$talleta['nimi'] = $this->request->getPost('nimi');
			$this->tuoteryhmaModel->save($talleta); //save-metodi valmiina Modelissa, tässä saa taulukon parametrinä

			//$tuoteryhmaModel->save([
			//	'nimi' => $this->request->getPost('nimi')
			//]);
			// Tässä siis sama asia, ilman $talleta-muuttujan käyttöä

			return redirect('admin/index'); //Uudelleenohjaus etusivulle. Määriteltävä Routesiin.

		}
	}

	public function poista($id) {
		$this->tuoteryhmaModel->poista($id);
		return redirect ('admin/index'); 
	}

	//--------------------------------------------------------------------

}
