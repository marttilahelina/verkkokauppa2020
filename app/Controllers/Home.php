<?php namespace App\Controllers;

use App\Models\TuoteryhmaModel;
use App\Models\TuoteModel; // yksi käsittelijä käyttää kaikkia tarvitsemiaan modeleja


class Home extends BaseController
{
	private $tuoteryhmaModel= null;
	private $tuoteModel = null;

	
    function __construct() {
	   $this->tuoteryhmaModel = new TuoteryhmaModel();
	   $this->tuoteModel = new TuoteModel();   
	}


	public function index()
	{
	   $data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
	   echo view('templates/header', $data); //huom. eri templatet ylläpitopuolelle ja asiakkaan puolelle!
	   echo view('etusivu');
	   echo view('templates/footer');	
	}

	public function tuotteet($tuoteryhma_id) 
	{
		$data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
		$data['tuotteet'] = $this->tuoteModel->haeTuoteryhmalla($tuoteryhma_id);
		echo view('templates/header', $data);
		echo view('tuotteet', $data);
		echo view('templates/footer');	
	}

    /**
    * Näyttää yksittäisen tuotteen.
    */
	public function tuote($tuote_id)
	{
		$data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
		$data['tuote'] = $this->tuoteModel->haeTuote($tuote_id); // huom. modelissa parametri nimeltään pelkkä id. Ei tarvi olla sama.
		echo view('templates/header', $data);
		echo view('tuote', $data);
		echo view('templates/footer');
	}

	//--------------------------------------------------------------------

}
